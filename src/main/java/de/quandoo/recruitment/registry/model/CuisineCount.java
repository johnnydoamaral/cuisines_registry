package de.quandoo.recruitment.registry.model;

public class CuisineCount extends Cuisine {

	private int count;

	public CuisineCount(String cuisineType, int count) {
		super(cuisineType);
		this.count = count;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
