package de.quandoo.recruitment.registry.model;

public class Cuisine {

	private String cuisineType;

	public Cuisine(String cuisineType) {
		this.cuisineType = cuisineType;
	}

	public String getCuisineType() {
		return cuisineType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cuisineType == null) ? 0 : cuisineType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cuisine other = (Cuisine) obj;
		if (cuisineType == null) {
			if (other.cuisineType != null)
				return false;
		} else if (!cuisineType.equals(other.cuisineType))
			return false;
		return true;
	}

}
