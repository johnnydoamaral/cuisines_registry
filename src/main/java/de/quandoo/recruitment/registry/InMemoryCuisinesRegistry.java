package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.CuisineCount;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.stat.Frequency;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

	private static InMemoryCuisinesRegistry instance;

	private static List<Customer> italianCuisineCustomers = new LinkedList<Customer>();
	private static List<Customer> frenchCuisineCustomers = new LinkedList<Customer>();
	private static List<Customer> germanCuisineCustomers = new LinkedList<Customer>();
	private static Map<String, List<Cuisine>> customerCuisines = new HashMap<String, List<Cuisine>>();

	private InMemoryCuisinesRegistry() {

	}

	public static InMemoryCuisinesRegistry getInstance() {
		if (instance == null) {
			return new InMemoryCuisinesRegistry();
		} else {
			return instance;
		}
	}

	@Override
	public void register(final Customer customer, final Cuisine cuisine) {
		if (!isCustomerAlreadyRegistered(customer, cuisine)) {
			switch (cuisine.getCuisineType().toUpperCase()) {
			case "FRENCH":
				frenchCuisineCustomers.add(customer);
				updateCustomerCuisines(customer.getCustomerId(), cuisine);
				break;
			case "GERMAN":
				germanCuisineCustomers.add(customer);
				updateCustomerCuisines(customer.getCustomerId(), cuisine);
				break;
			case "ITALIAN":
				italianCuisineCustomers.add(customer);
				updateCustomerCuisines(customer.getCustomerId(), cuisine);
				break;
			default:
				throw new IllegalArgumentException(
						"Unknown cuisine, please reach johnny@bookthattable.de to update the code.");
			}
		} else {
			System.err.println("Customer already follows that Cuisine.");
		}
	}

	@Override
	public List<Customer> cuisineCustomers(final Cuisine cuisine) {
		List<Customer> cuisineCustomers;
		if (cuisine != null) {
			switch (cuisine.getCuisineType().toUpperCase()) {
			case "FRENCH":
				cuisineCustomers = frenchCuisineCustomers;
				break;
			case "GERMAN":
				cuisineCustomers = germanCuisineCustomers;
				break;
			case "ITALIAN":
				cuisineCustomers = italianCuisineCustomers;
				break;
			default:
				cuisineCustomers = null;
				break;
			}
			return cuisineCustomers;
		}
		return null;
	}

	@Override
	public List<Cuisine> customerCuisines(final Customer customer) {
		return customerCuisines.get(customer.getCustomerId());
	}

	@Override
	public List<Cuisine> topCuisines(final int n) {
		List<CuisineCount> topCuisines = new ArrayList<CuisineCount>();
		topCuisines.add(new CuisineCount("french", frenchCuisineCustomers.size()));
		topCuisines.add(new CuisineCount("german", germanCuisineCustomers.size()));
		topCuisines.add(new CuisineCount("italian", italianCuisineCustomers.size()));
		Collections.sort(topCuisines, (n1, n2) -> n2.getCount() - n1.getCount());
		List<Cuisine> topCuisinesResult = new ArrayList<Cuisine>();
		for (int i = 0; i < n; i++) {
			topCuisinesResult.add(new Cuisine(topCuisines.get(i).getCuisineType()));
		}
		return topCuisinesResult;
	}

	private static void updateCustomerCuisines(String customerId, Cuisine cuisine) {
		List<Cuisine> cuisines = customerCuisines.get(customerId);
		if (cuisines == null) {
			cuisines = new ArrayList<Cuisine>();
			cuisines.add(cuisine);
			customerCuisines.put(customerId, cuisines);
		} else {
			cuisines.add(cuisine);
			customerCuisines.put(customerId, cuisines);
		}
	}

	private static boolean isCustomerAlreadyRegistered(Customer customer, Cuisine cuisine) {
		boolean customerAlreadyOnList = false;
		switch (cuisine.getCuisineType().toUpperCase()) {
		case "FRENCH":
			for (Customer c : frenchCuisineCustomers) {
				if (c.getCustomerId().equals(customer.getCustomerId()))
					customerAlreadyOnList = true;
			}
			break;
		case "GERMAN":
			for (Customer c : germanCuisineCustomers) {
				if (c.getCustomerId().equals(customer.getCustomerId()))
					customerAlreadyOnList = true;
			}
			break;
		case "ITALIAN":
			for (Customer c : italianCuisineCustomers) {
				if (c.getCustomerId().equals(customer.getCustomerId()))
					customerAlreadyOnList = true;
			}
			break;
		default:
			break;
		}
		return customerAlreadyOnList;
	}
}
