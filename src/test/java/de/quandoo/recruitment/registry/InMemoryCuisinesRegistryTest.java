package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InMemoryCuisinesRegistryTest {

	private InMemoryCuisinesRegistry cuisinesRegistry = InMemoryCuisinesRegistry.getInstance();

	@Before
	public void setUp() throws Exception {
		System.out.println("============================================================================");
	}

	@Test
	public void inMemoryCuisinesRegistryInstanceReceived() {
		assertNotNull(cuisinesRegistry);
		System.out.println("InMemoryCuisinesRegistry instance received!");
	}

	@Test
	public void testA_tryToRegisterItalianCuisine() {
		String cuisineType = "italian";
		cuisinesRegistry.register(new Customer(UUID.randomUUID().toString()), new Cuisine(cuisineType));
		cuisinesRegistry.register(new Customer(UUID.randomUUID().toString()), new Cuisine(cuisineType));
		cuisinesRegistry.register(new Customer(UUID.randomUUID().toString()), new Cuisine(cuisineType));

		List<Customer> italianCuisineCustomers = cuisinesRegistry.cuisineCustomers(new Cuisine("italian"));
		assertNotNull(italianCuisineCustomers);
		assertEquals(italianCuisineCustomers.size(), 3);
		System.out.println("Cuisine (italian) registered succesfully!");
	}

	@Test
	public void testB_tryToRegisterGermanCuisine() {
		String cuisineType = "german";
		cuisinesRegistry.register(new Customer(UUID.randomUUID().toString()), new Cuisine(cuisineType));
		cuisinesRegistry.register(new Customer(UUID.randomUUID().toString()), new Cuisine(cuisineType));

		List<Customer> germanCuisineCustomers = cuisinesRegistry.cuisineCustomers(new Cuisine("german"));
		assertNotNull(germanCuisineCustomers);
		assertEquals(germanCuisineCustomers.size(), 2);
		System.out.println("Cuisine (german) registered succesfully!");
	}

	@Test
	public void testC_tryToRegisterFrenchCuisine() {
		String cuisineType = "french";
		cuisinesRegistry.register(new Customer(UUID.randomUUID().toString()), new Cuisine(cuisineType));

		List<Customer> frenchCuisineCustomers = cuisinesRegistry.cuisineCustomers(new Cuisine("french"));
		assertNotNull(frenchCuisineCustomers);
		assertEquals(frenchCuisineCustomers.size(), 1);
		System.out.println("Cuisine (french) registered succesfully!");
	}

	@Test
	public void testD_retrieveTop2Cuisines() {
		int topNumber = 2;
		List<Cuisine> topCuisines = cuisinesRegistry.topCuisines(topNumber);
		assertNotNull("No cuisines received", topCuisines);
		System.out.println("Top " + topNumber + " cuisines:");
		for (Cuisine c : topCuisines) {
			System.out.println(c.getCuisineType());
		}
		assertEquals(topCuisines.get(0).getCuisineType().toLowerCase(), "italian");
		assertEquals(topCuisines.get(1).getCuisineType().toLowerCase(), "german");

	}

	@Test(expected = IllegalArgumentException.class)
	public void testF_tryToRegisterInvalidCuisine() {
		String cuisineType = "invalidCuisine";
		cuisinesRegistry.register(new Customer(UUID.randomUUID().toString()), new Cuisine(cuisineType));
	}

	@After
	public void tearDown() throws Exception {
		cuisinesRegistry = null;
		System.out.println("============================================================================");
	}

}